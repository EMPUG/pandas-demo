import os

import pandas as pd

INPUT_FILENAME = os.path.join('input', 'data.csv')
OUTPUT_FILENAME = os.path.join('output', 'data.json')


def main():
    df = pd.read_csv(INPUT_FILENAME, sep='|')
    print(df)
    print(df.iloc[2, 1])

    with open(OUTPUT_FILENAME, 'w') as f:
        f.write(df.to_json(indent=2))


if __name__ == '__main__':
    main()
