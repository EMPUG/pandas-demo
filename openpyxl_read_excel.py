import os

from openpyxl import load_workbook

INPUT_FILENAME = os.path.join('input', 'data.xlsx')


def main():
    # Read the Excel file
    wb = load_workbook(filename=INPUT_FILENAME)
    ws = wb['data']

    # Iterate through the rows and columns
    for row in ws.rows:
        for colnum in range(ws.max_column):
            val = row[colnum].value
            print(val)

    # Print a single value
    print(ws['D4'].value)


if __name__ == '__main__':
    main()
