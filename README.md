# Pandas Demo

The Pandas website describes it as "a fast, powerful, flexible and easy to use open
source data analysis and manipulation tool".

## NumPy

Pandas makes use of the Numpy's array class called `ndarray`, which has the alias
`array`. NumPy arrays are homogenous (all elements have the same type), and multidimensional.

We can think of a NumPy array as a list of lists:

        [[ 1., 0., 0.],
         [ 0., 1., 2.]]

NumPy provides a variety of datatypes, including `int32`, `float64` and
`complex64`.

NumPy provides constants such as `inf`, `nan` and `e`.

NumPy supports matrix math and linear algebra.

## Pandas

Pandas has two data structures, the Series and the DataFrame.

### Series

The Series is a one-dimensional labelled array. Examples:

        s1 = pd.Series([7, 5, np.nan, 3], ['a', 'b', 'c', 'd'], name='Series 1')
        s1['b']

        s2 = pd.Series([7, 5, np.nan, 3])
        s2[1]

        s3 = pd.Series({'a': 7, 'b': 5, 'c': np.nan, 'd': 3})
        s3['b']

        s1.dtype
        s1.name

### DataFrame

The DataFrame is like an array with an index. Each column has a header. Each
column is a series of the same data type.

        import pandas as pd
        import numpy as np

        df_index = pd.date_range("20210101", periods=5)
        df_columns = ['A', 'B', 'C']
        df_data = np.random.randn(5, 3)
        df = pd.DataFrame(df_data, index=df_index, columns=df_columns)

        df
        df.types
        df.index()
        df.columns()

        df.head()
        df.tail(2)
        df[1:3]
        df['20210102':'20210103']

        df['B']
        df.B

        df.iloc[2, 3]

        df.describe()
        df.sort_values(by="B")

### Input and Output

Pandas supports a variety of file formats. See the
[Pandas documentation](https://pandas.pydata.org/docs/user_guide/io.html) for
details. See `pandas_read_csv.py` and `pandas_read_excel.py` for examples using
CSV, JSON and Excel files.

## Installation

Create a Python 3 virtual environment and the required packages.

* For Linux and mac OS:

        python3 -m venv venv
        source venv/bin/activate
        pip install -r requirements.txt

* For Windows:

        python3 -m venv venv
        venv\Scripts\activate
        pip install -r requirements.txt

## Resources

Pandas: [https://pandas.pydata.org](https://pandas.pydata.org)

NumPy: [https://numpy.org](https://numpy.org)

openpyxl: [https://openpyxl.readthedocs.io/en/stable/index.html](https://openpyxl.readthedocs.io/en/stable/index.html)
