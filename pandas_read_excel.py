import os

import pandas as pd

INPUT_FILENAME = os.path.join('input', 'data.xlsx')


def main():
    # Read the Excel file
    df = pd.read_excel(INPUT_FILENAME, sheet_name='data')

    print(df)

    # Print a single value
    print(df.iloc[2, 3])


if __name__ == '__main__':
    main()
